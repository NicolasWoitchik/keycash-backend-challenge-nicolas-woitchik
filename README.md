# Keycash

Esta aplicação simples foi desenvolvida a partir de um teste chamado `backend_challenge_2`.

# Instalação

Requisitos mínimos.

- [Node.js](https://nodejs.org/) v12+

A aplicação foi criada utilizando `yarn` então para instalar os pacotes necessários basta rodar

```sh
cd keycash
yarn
```

### Executar a aplicação

Para executar a aplicação basta rodar o seguinte comando:

Docker:

```sh
yarn start
```

## Considerações

Creio que poderia melhorar na parte de listagem, aqueles filtros não estão de
acordo com algum Pattern pra isso.

## Desenvolvido por

[Nicolas Woitchik](https://github.com/NicolasWoitchik) - [nicolas@woitchik.com.br](mailto:nicolas@woitchik.com.br?subject=Keycash%20Desafio)
