# syntax=docker/dockerfile:1
FROM node:14.17.6
ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "yarn.lock", "ormconfig.example.json", "./"]

RUN yarn install --production
RUN cp ormconfig.example.json ormconfig.json
RUN yarn typeorm migration:run
RUN yarn build 

COPY . .

EXPOSE 3000
CMD [ "node", "dist/src/shared/infra/http/server.ts" ]
