import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateProperties1631838426260 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'properties',
        engine: 'InnoDB',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'bedrooms',
            type: 'integer',
          },
          {
            name: 'bathrooms',
            type: 'integer',
          },
          {
            name: 'price',
            type: 'integer',
          },
          {
            name: 'car_spaces',
            type: 'integer',
          },
          {
            name: 'living_area',
            type: 'integer',
          },
          {
            name: 'created_at',
            type: 'timestamp',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('properties');
  }
}
