import { Router } from 'express';

import propertiesRouter from '@modules/properties/infra/http/routes/properties.routes';

const routes = Router();

routes.get('/health', (req, res) => {
  res.json({ up: true });
});

routes.use('/properties', propertiesRouter);

export default routes;
