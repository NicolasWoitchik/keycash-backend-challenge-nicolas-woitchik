import { container } from 'tsyringe';

import './providers';

import IPropertiesRepository from '@modules/properties/repositories/IPropertiesRepository';
import PropertiesRepository from '@modules/properties/infra/typeorm/repositories/PropertiesRepository';

container.registerSingleton<IPropertiesRepository>(
  'PropertiesRepository',
  PropertiesRepository
);
