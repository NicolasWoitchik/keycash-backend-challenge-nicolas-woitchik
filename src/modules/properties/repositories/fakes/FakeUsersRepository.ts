import { uuid } from 'uuidv4';

import IPropertiesRepository from '../IPropertiesRepository';
import ICreatePropertyDTO from '../../dtos/ICreatePropertyDTO';
import ISearchPropertyDTO from '../../dtos/ISearchPropertyDTO';
import Property from '../../infra/typeorm/entities/Property';

class FakePropertiesRepository implements IPropertiesRepository {
  private properties: Property[] = [];

  public async list(data: ISearchPropertyDTO): Promise<Property[]> {
    return this.properties;
  }

  public async findById(id: string): Promise<Property | undefined> {
    const property = await this.properties.find(item => item.id === id);
    return property;
  }

  public async create(data: ICreatePropertyDTO): Promise<Property> {
    const property = new Property();

    Object.assign(property, { id: uuid() }, data);

    this.properties.push(property);
    return property;
  }

  public async save(property: Property): Promise<Property> {
    const findIndex = this.properties.findIndex(
      item => item.id === property.id
    );

    this.properties[findIndex] = Object.assign(
      property,
      { id: uuid() },
      property
    );

    return property;
  }
  public async delete(id: string): Promise<boolean> {
    const findIndex = this.properties.findIndex(item => item.id === id);

    delete this.properties[findIndex];

    return true;
  }
}

export default FakePropertiesRepository;
