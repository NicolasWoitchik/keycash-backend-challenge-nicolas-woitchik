import Property from '../infra/typeorm/entities/Property';

import ICreatePropertyDTO from '../dtos/ICreatePropertyDTO';
import ISearchPropertyDTO from '../dtos/ISearchPropertyDTO';

export default interface IPropertiesRepository {
  findById(id: string): Promise<Property | undefined>;
  list(data?: ISearchPropertyDTO): Promise<Property[]>;
  create(data: ICreatePropertyDTO): Promise<Property>;
  save(data: Property): Promise<Property>;
  delete(id: string): Promise<boolean>;
}
