import { Router } from 'express';
import { celebrate, Joi, Segments } from 'celebrate';

import PropertiesController from '../controllers/PropertiesController';

const propertiesController = new PropertiesController();

const propertiesRouter = Router();

propertiesRouter.get(
  '/',
  celebrate({
    [Segments.QUERY]: {
      bedrooms: Joi.number().optional(),
      bathrooms: Joi.number().optional(),
      price: Joi.number().optional(),
      living_area: Joi.number().optional(),
      car_spaces: Joi.number().optional(),
    },
  }),
  propertiesController.index
);

propertiesRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      bedrooms: Joi.number().required(),
      bathrooms: Joi.number().required(),
      living_area: Joi.number().required(),
      price: Joi.number().required(),
      car_spaces: Joi.number().required(),
    },
  }),
  propertiesController.create
);

propertiesRouter.get(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  propertiesController.show
);

propertiesRouter.put(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
    [Segments.BODY]: {
      name: Joi.string().required(),
      bedrooms: Joi.number().required(),
      bathrooms: Joi.number().required(),
      living_area: Joi.number().required(),
      price: Joi.number().required(),
      car_spaces: Joi.number().required(),
    },
  }),
  propertiesController.update
);

propertiesRouter.delete('/:id', propertiesController.delete);

export default propertiesRouter;
