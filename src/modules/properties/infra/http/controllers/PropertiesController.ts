import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { classToClass } from 'class-transformer';

import CreatePropertyService from '../../../services/CreatePropertyService';
import ShowPropertyService from '../../../services/ShowPropertyService';
import UpdatePropertyService from '../../../services/UpdatePropertyService';
import ListPropertiesService from '@modules/properties/services/ListPropertiesService';
import ISearchPropertyDTO from '@modules/properties/dtos/ISearchPropertyDTO';
import DeletePropertyService from '@modules/properties/services/DeletePropertyService';

class PropertiesController {
  public async index(req: Request, res: Response): Promise<Response> {
    const createProperty = container.resolve(ListPropertiesService);

    const bedrooms = req.query.bedrooms as number | number[] | undefined;

    const living_area = req.query.living_area as number | number[] | undefined;

    const price = req.query.price as number | number[] | undefined;

    const bathrooms = req.query.bathrooms as number | number[] | undefined;

    const car_spaces = req.query.car_spaces as number | number[] | undefined;

    let params: ISearchPropertyDTO = {
      bedrooms,
      living_area,
      price,
      bathrooms,
      car_spaces,
    };

    const properties = await createProperty.execute(params);

    return res.json(classToClass(properties));
  }

  public async create(req: Request, res: Response): Promise<Response> {
    const {
      bedrooms,
      bathrooms,
      living_area,
      name,
      price,
      car_spaces,
    } = req.body;

    const createProperty = container.resolve(CreatePropertyService);

    const property = await createProperty.execute({
      bedrooms,
      bathrooms,
      living_area,
      name,
      price,
      car_spaces,
    });

    return res.json(classToClass(property));
  }

  public async show(req: Request, res: Response): Promise<Response> {
    const property_id = req.params.id;

    const showProperty = container.resolve(ShowPropertyService);

    const property = await showProperty.execute({
      property_id,
    });

    return res.json(classToClass(property));
  }

  public async update(req: Request, res: Response): Promise<Response> {
    const property_id = req.params.id;
    const {
      bedrooms,
      bathrooms,
      living_area,
      name,
      price,
      car_spaces,
    } = req.body;

    const updateProperty = container.resolve(UpdatePropertyService);

    const property = await updateProperty.execute({
      property_id,
      bedrooms,
      bathrooms,
      living_area,
      name,
      price,
      car_spaces,
    });

    return res.json(classToClass(property));
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    const property_id = req.params.id;

    const deleteProperty = container.resolve(DeletePropertyService);

    const property = await deleteProperty.execute({
      property_id,
    });

    return res.json(classToClass(property));
  }
}

export default PropertiesController;
