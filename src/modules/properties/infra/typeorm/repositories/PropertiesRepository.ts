import {
  Equal,
  FindConditions,
  FindManyOptions,
  getRepository,
  In,
  Repository,
} from 'typeorm';
import { types } from 'util';

import Property from '../entities/Property';
import ICreatePropertyDTO from '../../../dtos/ICreatePropertyDTO';
import IPropertiesRepository from '../../../repositories/IPropertiesRepository';
import ISearchPropertyDTO from '@modules/properties/dtos/ISearchPropertyDTO';

class PropertiesRepository implements IPropertiesRepository {
  private ormRepository: Repository<Property>;

  constructor() {
    this.ormRepository = getRepository(Property);
  }
  public async list(data?: ISearchPropertyDTO): Promise<Property[]> {
    let bathrooms = data?.bathrooms;
    let bedrooms = data?.bedrooms;
    let car_spaces = data?.car_spaces;
    let living_area = data?.living_area;
    let price = data?.price;

    const where: FindConditions<Property> = {};
    /**
     * OK, confesso que isso não está de acordo com o que deveria ser feito
     * algumas alternativas seria percorrer o parametro "data" usando um
     * Object.keys(data) porém isso traria alguns problemas caso houvesse
     * campos não listados, (ex. deleted_at).
     */
    if (where.bathrooms)
      where.bathrooms = Array.isArray(bathrooms)
        ? In(bathrooms)
        : Equal(bathrooms);

    if (where.bedrooms)
      where.bedrooms = Array.isArray(bedrooms) ? In(bedrooms) : Equal(bedrooms);

    if (where.car_spaces)
      where.car_spaces = Array.isArray(car_spaces)
        ? In(car_spaces)
        : Equal(car_spaces);

    if (where.living_area)
      where.living_area = Array.isArray(living_area)
        ? In(living_area)
        : Equal(living_area);

    if (where.price)
      where.price =
        where.price && Array.isArray(price) ? In(price) : Equal(price);

    console.log(where);

    return this.ormRepository.find(where);
  }

  public async findById(id: string): Promise<Property | undefined> {
    const property = await this.ormRepository.findOne(id);
    return property;
  }

  public async findByEmail(email: string): Promise<Property | undefined> {
    const property = await this.ormRepository.findOne({
      where: { email },
    });
    return property;
  }

  public async create(data: ICreatePropertyDTO): Promise<Property> {
    const property = this.ormRepository.create(data);

    await this.ormRepository.save(property);

    return property;
  }

  public async save(property: Property): Promise<Property> {
    return this.ormRepository.save(property);
  }

  public async delete(id: string): Promise<boolean> {
    await this.ormRepository.delete(id);
    return true;
  }
}

export default PropertiesRepository;
