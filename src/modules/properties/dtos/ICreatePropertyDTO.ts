export default interface ICreatePropertyDTO {
  name: string;
  bedrooms: number;
  bathrooms: number;
  price: number;
  living_area: number;
  car_spaces: number;
}
