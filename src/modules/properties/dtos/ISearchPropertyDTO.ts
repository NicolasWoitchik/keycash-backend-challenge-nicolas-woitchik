export default interface ISearchPropertyDTO {
  bedrooms?: number | number[];
  bathrooms?: number | number[];
  price?: number | number[];
  living_area?: number | number[];
  car_spaces?: number | number[];
}
