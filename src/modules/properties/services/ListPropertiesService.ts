import { injectable, inject } from 'tsyringe';
import ISearchPropertyDTO from '../dtos/ISearchPropertyDTO';

import Property from '../infra/typeorm/entities/Property';
import IPropertiesRepository from '../repositories/IPropertiesRepository';

@injectable()
class ListPropertiesService {
  constructor(
    @inject('PropertiesRepository')
    private propertiesRepository: IPropertiesRepository
  ) {}

  public async execute(params?: ISearchPropertyDTO): Promise<Property[]> {
    const properties = await this.propertiesRepository.list(params);

    return properties;
  }
}

export default ListPropertiesService;
