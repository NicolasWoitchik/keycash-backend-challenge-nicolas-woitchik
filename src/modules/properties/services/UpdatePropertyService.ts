import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import Property from '../infra/typeorm/entities/Property';
import IPropertiesRepository from '../repositories/IPropertiesRepository';

interface IRequest {
  property_id: string;
  bedrooms: number;
  bathrooms: number;
  name: string;
  living_area: number;
  price: number;
  car_spaces: number;
}

@injectable()
class UpdatePropertyService {
  constructor(
    @inject('PropertiesRepository')
    private propertiesRepository: IPropertiesRepository
  ) {}

  public async execute({
    property_id,
    bedrooms,
    bathrooms,
    name,
    living_area,
    price,
    car_spaces,
  }: IRequest): Promise<Property> {
    const property = await this.propertiesRepository.findById(property_id);

    if (!property) {
      throw new AppError('Property not found');
    }

    Object.assign(property, {
      bads: bedrooms,
      bathrooms,
      name,
      living_area,
      price,
      car_spaces,
    });

    return this.propertiesRepository.save(property);
  }
}

export default UpdatePropertyService;
