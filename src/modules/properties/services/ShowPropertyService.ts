import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import Property from '../infra/typeorm/entities/Property';
import IPropertiesRepository from '../repositories/IPropertiesRepository';

interface IRequest {
  property_id: string;
}

@injectable()
class ShowPropertyService {
  constructor(
    @inject('PropertiesRepository')
    private propertiesRepository: IPropertiesRepository
  ) {}

  public async execute({ property_id }: IRequest): Promise<Property> {
    const property = await this.propertiesRepository.findById(property_id);

    if (!property) {
      throw new AppError('Property not found', 401);
    }

    return property;
  }
}

export default ShowPropertyService;
