import { injectable, inject } from 'tsyringe';

import Property from '../infra/typeorm/entities/Property';
import IPropertiesRepository from '../repositories/IPropertiesRepository';
import ICreatePropertyDTO from '../dtos/ICreatePropertyDTO';

@injectable()
class CreatePropertyService {
  constructor(
    @inject('PropertiesRepository')
    private propertiesRepository: IPropertiesRepository
  ) {}

  public async execute({
    bedrooms,
    bathrooms,
    price,
    living_area,
    car_spaces,
    name,
  }: ICreatePropertyDTO): Promise<Property> {
    // validate if another property exists

    const property = await this.propertiesRepository.create({
      bedrooms,
      bathrooms,
      name,
      living_area,
      car_spaces,
      price,
    });

    return property;
  }
}

export default CreatePropertyService;
